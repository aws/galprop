GALPROP is a numerical package for Galactic cosmic-ray propagation and associated electromagetic emission including gamma rays and synchrotron radiation.

This is version 54 based on the original public GALPROP version and extensively updated.
It has been maintained up to now at sourceforge https://sourceforge.net/projects/galprop.

Further developments will be maintained here with gitlab hosted by the Max-Planck Society MPCDF.

This starts with the last revision r2766 on sourceforge; see there for details.

For further information and related projects see https://www.mpe.mpg.de/~aws/propagate.html.

Current version (r2766) includes
* accurate propagation scheme
* polarized synchrotron
* free-free emission and absorption
* new convection models
* anisotropic diffusion
* primary positrons
* free-escape boundary conditions
* new hadronic gamma-ray production models
* new injection spectral breaks
* upwards compatible with latest HEALPix
* improved HEALPix skymap format
* full reference output for a sample run
* deuterium production by pp fusion
* Jansson and Farrar 2012 B-field
* hadronic energy losses for p and He

 
Full details can be found in the Explanatory Supplement (included in the package) and details of installation and the change log are in the package README.

This development of GALPROP v54 had been downloaded almost 900 times since the start in 2015 up to change to git repository.

Please inform me (aws@mpe.mpg.de or post a message on the blog) if you download from gitlab since it does not record download statistics yet.


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Note added 3 Nov 2020: DM extensions to v54 from Andrey Egorov are at
https://github.com/a-e-egorov/GALPROP_DM

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Older versions, datasets etc: http://galprop.stanford.edu .

See also

http://www.mpe.mpg.de/~aws/propagate.html

https://gitlab.mpcdf.mpg.de/aws/synchrotron General-purpose synchrotron routines with full Bessel functions and including polarization.

https://gitlab.mpcdf.mpg.de/aws/stellarics  StellarICS: Inverse Compton emission from the sun and stars, including general-purpose inverse Compton scattering routines.

https://sourceforge.net/projects/galplot  A plotting package for GALPROP output.

http://sourceforge.net/projects/galpropskymapco  Conversion of older HEALPix format to current format

https://sourceforge.net/projects/gcrconvert      Convert GALPROP cosmic-ray files to a format viewable with fv

