AC_DEFUN([GAL_WITH_FITSDATA],
[_GAL_WITH_PACKAGE([fitsdata],
  [AC_HELP_STRING([--with-fitsdata],
                  [use fitsdata (default: ARG or FITSDATA_PATH or search)])],
  [/usr /usr/local $FITSDATA_PATH],
  [])
if test x$gal_cv_root_fitsdata != xno
then
  FITSDATA_PATH=$gal_cv_root_fitsdata
#  AC_SUBST(GALDEFDATA_HOME)
  AC_SUBST(FITSDATA_PATH) 
  AC_DEFINE([HAVE_FITSDATA], 1)
else
  GAL_MISSING([FITSDATA])
fi
AH_TEMPLATE([HAVE_FITSDATA], [Define to 1 if fits data path is provided at configuration time])
])

AC_DEFUN([GAL_WITH_GALDEF],
[_GAL_WITH_PACKAGE([galdef],
  [AC_HELP_STRING([--with-galdef],
                  [use galdef (default: ARG or GALDEF_PATH or search)])],
  [/usr /usr/local $GALDEF_PATH],
  [galdef_54_77Xvarh7S])
if test x$gal_cv_root_galdef != xno
then
  GALDEF_PATH=$gal_cv_root_galdef
#  AC_SUBST(GALDEFDATA_HOME)
  AC_SUBST(GALDEF_PATH) 
  AC_DEFINE([HAVE_GALDEF], 1)
else
  GAL_MISSING([GALDEF])
fi
AH_TEMPLATE([HAVE_GALDEF], [Define to 1 if galdef path is provided at configuration time])
])

# GAL_WITH_GSL
#   If GSL found, provide substitution variable
#     GSL_HOME
#     GSL_CFLAGS
#     GSL_LIBS
#
AC_DEFUN([GAL_WITH_GSL],
[_GAL_WITH_PACKAGE([gsl],
  [AC_HELP_STRING([--with-gsl],
                  [use gsl (default: ARG or GSL_HOME or search)])],
  [/usr /usr/local $GSL_HOME],
  [include/gsl/gsl_types.h])
if test x$gal_cv_root_gsl != xno
then
  GSL_HOME=$gal_cv_root_gsl
  AC_SUBST(GSL_HOME)
  GSL_CPPFLAGS=-I$GSL_HOME/include
  AC_SUBST(GSL_CPPFLAGS)
  GSL_LIBS="-L$GSL_HOME/lib -lgsl -lgslcblas -lm"
  AC_SUBST(GSL_LIBS)
  AC_DEFINE([HAVE_GSL], 1)
else
  GAL_MISSING([GSL])
fi
AH_TEMPLATE([HAVE_GSL], [Define to 1 if gsl is available on the system])
])

# GAL_REQUIRE_CFITSIO
#   Require that CFITSIO is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([GAL_REQUIRE_GSL],
[AC_REQUIRE([GAL_WITH_GSL])
if test x$gal_cv_root_gsl = xno
then
  GAL_MISSING_REQUIRED([GSL])
fi])

# GAL_WITH_CFITSIO
#   If CFITSIO found, provide substitution variable
#     CFITSIO_HOME
#     CFITSIO_CPPFLAGS
#     CFITSIO_LIBS
#
AC_DEFUN([GAL_WITH_CFITSIO],
[_GAL_WITH_PACKAGE([cfitsio],
  [AC_HELP_STRING([--with-cfitsio],
                  [use cfitsio (default: ARG or CFITSIO_HOME or search)])],
  [/usr /usr/local $CFITSIO_HOME],
  [include/fitsio.h])
if test x$gal_cv_root_cfitsio != xno
then
  CFITSIO_HOME=$gal_cv_root_cfitsio
  AC_SUBST(CFITSIO_HOME)
  CFITSIO_CPPFLAGS=-I$CFITSIO_HOME/include
  AC_SUBST(CFITSIO_CPPFLAGS)
  CFITSIO_LIBS="-L$CFITSIO_HOME/lib -lcfitsio"
  AC_SUBST(CFITSIO_LIBS)
  AC_DEFINE([HAVE_CFITSIO], 1)
else
  GAL_MISSING([CFITSIO])
fi
AH_TEMPLATE([HAVE_CFITSIO], [Define to 1 if cfitsio is available on the system])
])

# GAL_REQUIRE_CFITSIO
#   Require that CFITSIO is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([GAL_REQUIRE_CFITSIO],
[AC_REQUIRE([GAL_WITH_CFITSIO])
if test x$gal_cv_root_cfitsio = xno
then
  GAL_MISSING_REQUIRED([CFITSIO])
fi])

# GAL_WITH_CCFITS
#   If CCFITS found, provide substitution variable
#     CCFITS_HOME
#     CCFITS_CPPFLAGS
#     CCFITS_LIBS
#
AC_DEFUN([GAL_WITH_CCFITS],
[_GAL_WITH_PACKAGE([ccfits],
  [AC_HELP_STRING([--with-ccfits],
                  [use ccfits (default: ARG or CCFITS_HOME or search)])],
  [/usr /usr/local $CCFITS_HOME],
  [include/CCfits/CCfits])
if test x$gal_cv_root_ccfits != xno
then
  CCFITS_HOME=$gal_cv_root_ccfits
  AC_SUBST(CCFITS_HOME)
  CCFITS_CPPFLAGS=-I$CCFITS_HOME/include
  AC_SUBST(CCFITS_CPPFLAGS)
  CCFITS_LIBS="-L$CCFITS_HOME/lib -lCCfits"
  AC_SUBST(CCFITS_LIBS)
  AC_DEFINE([HAVE_CCFITS], 1)
else
  GAL_MISSING([CCFITS])
fi
AH_TEMPLATE([HAVE_CCFITS], [Define to 1 if ccfits is available on the system])
])

# GAL_REQUIRE_CCFITS
#   Require that CCFITS is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([GAL_REQUIRE_CCFITS],
[AC_REQUIRE([GAL_WITH_CCFITS])
if test x$gal_cv_root_ccfits = xno
then
  GAL_MISSING_REQUIRED([CCFITS])
fi])


# GAL_WITH_HEALPIX
#   If HEALPIX found, provide substitution variable
#     HEALPIX_HOME
#     HEALPIX_CPPFLAGS
#     HEALPIX_LIBS
#
AC_DEFUN([GAL_WITH_HEALPIX],
[_GAL_WITH_PACKAGE([healpix],
  [AC_HELP_STRING([--with-healpix],
                  [use healpix (default: ARG or HEALPIX_HOME or search)])],
  [/usr /usr/local $HEALPIX_HOME],
  [include/healpix_base.h])
if test x$gal_cv_root_healpix != xno
then
  HEALPIX_HOME=$gal_cv_root_healpix
  AC_SUBST(HEALPIX_HOME)
  HEALPIX_CPPFLAGS=-I$HEALPIX_HOME/include
  AC_SUBST(HEALPIX_CPPFLAGS)
  HEALPIX_LIBS="-L$HEALPIX_HOME/lib -lhealpix_cxx -lcxxsupport -lfftpack"
  AC_SUBST(HEALPIX_LIBS)
  AC_DEFINE([HAVE_HEALPIX], 1)
else
  GAL_MISSING([HEALPIX])
fi
AH_TEMPLATE([HAVE_HEALPIX], [Define to 1 if healpix is available on the system])
])

# GAL_REQUIRE_HEALPIX
#   Require that HEALPIX is installed, if not, configuration fails
#   (No version checking, could test features...)
#
AC_DEFUN([GAL_REQUIRE_HEALPIX],
[AC_REQUIRE([GAL_WITH_HEALPIX])
if test x$gal_cv_root_healpix = xno
then
  GAL_MISSING_REQUIRED([HEALPIX])
fi])

# GAL_WITH_CLHEP
#   If CLHEP found, provide substitution variable
#     CLHEPHOME
#     CLHEP_CPPFLAGS
#     CLHEP_LDFLAGS
#
AC_DEFUN([GAL_WITH_CLHEP],
[_GAL_WITH_PACKAGE([clhep],
  [AC_HELP_STRING([--with-clhep],
                  [use clhep (default: ARG or CLHEPHOME or search)])],
  [/usr /usr/local /opt/local /usr/local/CLHEP $CLHEPHOME],
  [include/CLHEP/Evaluator/Evaluator.h])
if test x$gal_cv_root_clhep != xno
then
  CLHEPHOME=$gal_cv_root_clhep
  AC_SUBST(CLHEPHOME)
  CLHEP_CPPFLAGS=-I$CLHEPHOME/include
  AC_SUBST(CLHEP_CPPFLAGS)
  CLHEP_LDFLAGS="-L$CLHEPHOME/lib -lCLHEP"
  AC_SUBST(CLHEP_LDFLAGS)
  AC_DEFINE([HAVE_CLHEP], 1)
else
  GAL_MISSING([CLHEP])
fi
AH_TEMPLATE([HAVE_CLHEP], [Define to 1 if CLHEP is available on the system])
])

# GAL_REQUIRE_CLHEP
#   Require that CLHEP is installed, if not, configuration fails
#
# Attempt to distinguish between 1.8, 1.9 and 2.0 series of CLHEP :
#  1.9 : only 1.9 has $CLHEPHOME/include/CLHEP/config/defs.h. 
#        Geant4.8.1 is tested against 1.9.2.3.
#  2.0 : no $CLHEPHOME/include/CLHEP/config/defs.h file, BUT there is
#        a $CLHEPHOME/include/CLHEP/Evaluator/defs.h file.
#  1.8 : has no defs.h files at all
#
AC_DEFUN([GAL_REQUIRE_CLHEP],
[AC_REQUIRE([GAL_WITH_CLHEP])
if test x$gal_cv_root_clhep = xno
then
  GAL_MISSING_REQUIRED([CLHEP])
fi
AC_CHECK_FILE([$CLHEPHOME/include/CLHEP/config/defs.h],
              [AC_MSG_NOTICE([CLHEP 1.9.x series found as default.  This version does not work with Offline. Please install CLHEP 1.8.x or 2.x series.])]
              [GAL_MISSING_REQUIRED([CLHEP])],
              [AC_CHECK_FILE([$CLHEPHOME/include/CLHEP/Evaluator/defs.h],
                             [AC_MSG_NOTICE([Using CLHEP 2.0 series])]
                             [AC_DEFINE([CLHEP_V2_0], 1)],
                               [AC_MSG_NOTICE([Using CLHEP 1.8 series])] 
                               [AC_DEFINE([CLHEP_V1_8], 1)]  )])
AH_TEMPLATE([CLHEP_V1_8], [Define to 1 if CLHEP version =  1.8])
AH_TEMPLATE([CLHEP_V2_0], [Define to 1 if CLHEP version >= 2])
])

dnl Check whether the target supports __sync_fetch_and_add.
AC_DEFUN([LIBGFOR_CHECK_SYNC_FETCH_AND_ADD], [
  AC_CACHE_CHECK([whether the target supports __sync_fetch_and_add],
                 have_sync_fetch_and_add, [
  AC_TRY_LINK([int foovar = 0;], [
if (foovar <= 0) return __sync_fetch_and_add (&foovar, 1);
if (foovar > 10) return __sync_add_and_fetch (&foovar, -1);],
              have_sync_fetch_and_add=yes, have_sync_fetch_and_add=no)])
  if test $have_sync_fetch_and_add = yes; then
    AC_DEFINE(HAVE_SYNC_FETCH_AND_ADD, 1,
              [Define to 1 if the target supports __sync_fetch_and_add])
  fi])

# GAL_CHECK_DARWIN
# Check for Darwin
#
AC_DEFUN([GAL_CHECK_DARWIN],
[gal_uname_system=`uname -s`
if test "x$gal_uname_system" = xDarwin
then
 gal_dylib_extension=dylib
 echo Configuring for Darwin
 PLATFORM_LD_LIBRARY_PATH=DYLD_LIBRARY_PATH
else
 gal_dylib_extension=so
 PLATFORM_LD_LIBRARY_PATH=LD_LIBRARY_PATH
fi
AC_SUBST(PLATFORM_LD_LIBRARY_PATH)
AM_CONDITIONAL([DARWIN], [test "x$gal_uname_system" = xDarwin])
])

################################################################
# Check whether the compiler accepts the __m128d type
# ----------------------------------
AC_DEFUN([GAL_M128D],
[AC_MSG_CHECKING([whether the compiler accepts the __m128d type])
AC_COMPILE_IFELSE( AC_LANG_PROGRAM( [
#include <emmintrin.h>
], [
__m128d a __attribute__((aligned(16)));
 ]), 
 [AC_DEFINE(HAVE_M128D, 1, [compiler supports the m128d type]) [acx_m128d=yes]], [acx_m128d=no])
AC_MSG_RESULT($acx_m128d)])

#################################################################
# Enables architecture specific code
AC_DEFUN([GAL_ARCH],
[
AC_REQUIRE([AC_CANONICAL_HOST])

vector=no
assembler=no

case "${host}" in
x86_64*)
GAL_M128D
oct_arch=x86_64
vector=$acx_m128d
assembler=no
AC_DEFINE(OCT_ARCH_X86_64, 1, [This an x86_64 system])
;;
i?86*)
GAL_M128D
vector=$acx_m128d
oct_arch=x86

if test x$vector = xyes ; then
# We allow explicit disabling of SSE2
ac_enable_sse2=no
AC_ARG_ENABLE(sse2, AS_HELP_STRING([--enable-sse2], [Enable the use of SSE2 instructions (x86)]), 
	[ac_enable_sse2=${enableval}])

if test x"${ac_enable_sse2}" = x"no"; then
vector=disabled
fi
fi
AC_DEFINE(OCT_ARCH_X86_64, 1, [This an x86 system])
;;	
ia64*)
oct_arch=ia64
assembler=yes
AC_DEFINE(OCT_ARCH_IA64, 1, [This an Itanium system])
;;
sparc*)
oct_arch=sparc
AC_DEFINE(OCT_ARCH_SPARC, 1, [This a Sparc system])
;;
alphaev*)
oct_arch=alpha
AC_DEFINE(OCT_ARCH_ALPHA, 1, [This an Alpha system])
;;
mips*)
oct_arch=mips
AC_DEFINE(OCT_ARCH_MIPS, 1, [This a MIPS system])
;;
powerpc*)
oct_arch=powerpc
AC_DEFINE(OCT_ARCH_POWERPC, 1, [This a PowerPC system])
;;
*)
oct_arch=unknown
;;
esac

ac_enable_sse2=yes
AC_ARG_ENABLE(sse2, AS_HELP_STRING([--disable-sse2], [Disable the use of SSE2 instructions (x86_64)]), 
	[ac_enable_sse2=${enableval}])

if test x"${ac_enable_sse2}" = x"no"; then
vector=disabled
fi

# We allow explicit disabling of assembler
#ac_enable_assembler=yes
#AC_ARG_ENABLE(assembler, AS_HELP_STRING([--disable-assembler], [Disable the use of assembler code (ia64)]), 
#	[ac_enable_assembler=${enableval}])

#if test x"${ac_enable_assembler}" = x"no"; then
#assembler=disabled
#fi

AC_DEFINE_UNQUOTED(OCT_ARCH, $oct_arch, [The architecture of this system])

#AM_CONDITIONAL(COMPILE_AS, test x$assembler = xyes)
AM_CONDITIONAL(COMPILE_VEC, test x$vector = xyes)

if test x$vector = xyes ; then
AC_DEFINE(HAVE_VEC, 1, [Define to 1 if vectorial routines are to be compiled])
fi

#if test x$assembler = xyes ; then
#AC_DEFINE(HAVE_AS, 1, [Define to 1 if assembler routines are to be compiled])
#fi

AC_MSG_NOTICE([ Architecture specific code:
***************************
This is a $oct_arch processor:
vectorial code: $vector 
***************************])
])

AC_DEFUN([GAL_CXXFLAGS],
[
AC_REQUIRE([AC_CANONICAL_HOST])

if test -z "${CXXFLAGS}"; then
  case "${CXX}" in
    g++*)
      CXXFLAGS="-g -pipe -O3 -Wall"
      ;;
    opencc*|openCC*)
      CXXFLAGS="-g -pipe -O3 -Wall -apo -ffast-math -mso"
      ;;
    icc*|icpc*)
      case "${host}" in
        x86_64*)
          CXXFLAGS="-g -pipe -O3 -xhost -no-ipo -parallel -Wall -gcc"
          ;;
        i?86*linux*)
          CXXFLAGS="-g -pipe -O3 -xhost -no-ipo -parallel -Wall -gcc"
          a=`echo $host | sed "s/^i//" | sed "s/86.*//"`
          if test "$a" -gt 5 ; then
            CXXFLAGS="$CXXFLAGS"
          fi
          ;;	
        ia64*)
          CXXFLAGS="-g -O3 -ip -IPF_fp_relaxed -ftz -fpp -u -align all -pad -gcc"
         ;;
      esac
      ;;
    sun*)
      case "${host}" in
        i?86*linux*|x86_64*)
          CXXFLAGS="-fast -xprefetch -xvector=simd"
          ;;
        sparc*)
          CXXFLAGS="-fast"
          ;;
      esac
      ;;
    *)
       CXXFLAGS="-O3"
       ;;
  esac
fi
#AC_MSG_NOTICE([Using CXXFLAGS="$CXXFLAGS"])
])

AC_DEFUN([GAL_FFLAGS],
[
AC_REQUIRE([AC_CANONICAL_HOST])

if test -z "${FFLAGS}"; then
  case "${F77}" in
    gfortran*)
      FFLAGS="-g -pipe -O3 -Wall"
      ;;
    openf9*)
      FFLAGS="-g -pipe -O3 -Wall -apo -ffast-math -mso"
      ;;
    g95*)
      FFLAGS="-g -pipe -O3 -Wall"
      ;;
    efc*|ifc*|ifort*)
      case "${host}" in
        x86_64*)
          FFLAGS="-g -O3 -xhost -no-ipo -parallel -gcc"
          ;;
        i?86*linux*)
          FFLAGS="-g -O3 -xhost -no-ipo -parallel -gcc"
          a=`echo $host | sed "s/^i//" | sed "s/86.*//"`
          if test "$a" -gt 5 ; then
            FFLAGS="$FFLAGS"
          fi
          ;;	
        ia64*)
          FFLAGS="-O3 -ip -IPF_fp_relaxed -ftz -fpp -u -align all -pad -gcc"
         ;;
      esac
      ;;
    sun*)
      case "${host}" in
        i?86*linux*|x86_64*)
          FFLAGS="-fast -xprefetch -xvector=simd"
          ;;
        sparc*)
          FFLAGS="-fast"
          ;;
      esac
      ;;
    *)
       FFLAGS="-O3"
       ;;
  esac
fi
#AC_MSG_NOTICE([Using FFLAGS="$FFLAGS"])
])

#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#USA.
#
#As a special exception, the respective Autoconf Macro's copyright
#owner gives unlimited permission to copy, distribute and modify the
#configure scripts that are the output of Autoconf when processing the
#Macro. You need not follow the terms of the GNU General Public License
#when using or distributing such scripts, even though portions of the
#text of the Macro appear in them. The GNU General Public License (GPL)
#does govern all other use of the material that constitutes the
#Autoconf Macro.
#
#This special exception to the GPL applies to versions of the Autoconf
#Macro released by the Autoconf Macro Archive. When you make and
#distribute a modified version of the Autoconf Macro, you may extend
#this special exception to the GPL to apply to your modified version as
#well.
#
# http://autoconf-archive.cryp.to/ax_openmp.html
#

AC_DEFUN([GAL_OPENMP], [
AC_PREREQ(2.59) dnl for _AC_LANG_PREFIX

AC_CACHE_CHECK([for OpenMP flag of _AC_LANG compiler], ax_cv_[]_AC_LANG_ABBREV[]_openmp, [save[]_AC_LANG_PREFIX[]FLAGS=$[]_AC_LANG_PREFIX[]FLAGS
ax_cv_[]_AC_LANG_ABBREV[]_openmp=unknown
# Flags to try:  -fopenmp (gcc), -openmp (icc), -mp (SGI & PGI),
#                -xopenmp (Sun), -omp (Tru64), -qsmp=omp (AIX), none
ax_openmp_flags="-openmp -mp=numa -mp=nonuma -mp -xopenmp -omp -qsmp=omp -fopenmp none"
if test "x$OPENMP_[]_AC_LANG_PREFIX[]FLAGS" != x; then
  ax_openmp_flags="$OPENMP_[]_AC_LANG_PREFIX[]FLAGS $ax_openmp_flags"
fi
for ax_openmp_flag in $ax_openmp_flags; do
  case $ax_openmp_flag in
    none) []_AC_LANG_PREFIX[]FLAGS="$save[]_AC_LANG_PREFIX[] $_AC_LANG_PREFIX[]FLAGS" ;;
    *) []_AC_LANG_PREFIX[]FLAGS="$save[]_AC_LANG_PREFIX[]FLAGS $ax_openmp_flag";;
  esac
  AC_TRY_LINK_FUNC(omp_set_num_threads,
        [ax_cv_[]_AC_LANG_ABBREV[]_openmp=$ax_openmp_flag; break])
done
[]_AC_LANG_PREFIX[]FLAGS=$save[]_AC_LANG_PREFIX[]FLAGS
])
if test "x$ax_cv_[]_AC_LANG_ABBREV[]_openmp" = "xunknown"; then
  m4_default([$2],:)
else
  if test "x$ax_cv_[]_AC_LANG_ABBREV[]_openmp" != "xnone"; then
    OPENMP_[]_AC_LANG_PREFIX[]FLAGS=$ax_cv_[]_AC_LANG_ABBREV[]_openmp
  fi
  m4_default([$1], [AC_DEFINE(HAVE_OPENMP,1,[Define if OpenMP is enabled])])
fi
])dnl GAL_OPENMP

AC_DEFUN([GAL_OPENMP_FORTRAN], [
AC_PREREQ(2.59) dnl for _AC_LANG_PREFIX

AC_CACHE_CHECK([for OpenMP flag of _AC_LANG compiler], ax_cv_[]_AC_LANG_ABBREV[]_openmp, [save[]_AC_LANG_PREFIX[]FLAGS=$[]_AC_LANG_PREFIX[]FLAGS
ax_cv_[]_AC_LANG_ABBREV[]_openmp=unknown
# Flags to try:  -fopenmp (gcc), -openmp (icc), -mp (SGI & PGI),
#                -xopenmp (Sun), -omp (Tru64), -qsmp=omp (AIX), none
ax_openmp_flags="-openmp -mp=numa -mp=nonuma -mp -xopenmp -omp -qsmp=omp -fopenmp none"
if test "x$OPENMP_[]_AC_LANG_PREFIX[]FLAGS" != x; then
  ax_openmp_flags="$OPENMP_[]_AC_LANG_PREFIX[]FLAGS $ax_openmp_flags"
fi
for ax_openmp_flag in $ax_openmp_flags; do
  case $ax_openmp_flag in
    none) []_AC_LANG_PREFIX[]FLAGS="$save[]_AC_LANG_PREFIX[] $_AC_LANG_PREFIX[]FLAGS" ;;
    *) []_AC_LANG_PREFIX[]FLAGS="$save[]_AC_LANG_PREFIX[]FLAGS $ax_openmp_flag";;
  esac
  AC_TRY_LINK([#include <omp.h>],[      call omp_set_num_threads(1)],
        [ax_cv_[]_AC_LANG_ABBREV[]_openmp=$ax_openmp_flag; break])
done
[]_AC_LANG_PREFIX[]FLAGS=$save[]_AC_LANG_PREFIX[]FLAGS
])
if test "x$ax_cv_[]_AC_LANG_ABBREV[]_openmp" = "xunknown"; then
  m4_default([$2],:)
else
  if test "x$ax_cv_[]_AC_LANG_ABBREV[]_openmp" != "xnone"; then
    OPENMP_[]_AC_LANG_PREFIX[]FLAGS=$ax_cv_[]_AC_LANG_ABBREV[]_openmp
  fi
  m4_default([$1], [AC_DEFINE(HAVE_OPENMP,1,[Define if OpenMP is enabled])])
fi
])dnl GAL_OPENMP_FORTRAN

# GAL_INIT
#   Initial code for extra scripts to generate
#
AC_DEFUN([GAL_INIT],
[gal_missing=
gal_missing_required=
])


# GAL_MISSING
#   Deal with missing pacakges
#
AC_DEFUN([GAL_MISSING],
[gal_missing="$gal_missing $1"
])


# GAL_MISSING_REQUIRED
#   Deal with missing pacakges
#
AC_DEFUN([GAL_MISSING_REQUIRED],
[gal_missing_required="$gal_missing_required $1"
])


# GAL_CHECK_MISSING_REQUIRED_PACKAGES
#   Code to test if we are missing any required packages.
#   Calls GAL_FINISH if this is the case.
#
AC_DEFUN([GAL_CHECK_MISSING_REQUIRED_PACKAGES],
[
if test "x$gal_missing_required" != x
then
  GAL_FINISH
fi  
])

# GAL_FINISH
#   Final code after all tests for external packages
#   Aborts execution if we are missing any required packages.
#
AC_DEFUN([GAL_FINISH],
[cat >missing-packages <<EOF
#! /bin/sh
if test x\$[]1 = x--required
then
  echo $gal_missing_required
else
  echo $gal_missing
fi
EOF
chmod a+x missing-packages
if test "x$gal_missing" != x
then
  AC_MSG_NOTICE([Optional packages not found or deactivated on this system:])
  AC_MSG_NOTICE([    $gal_missing])
fi
if test "x$gal_missing_required" != x
then
  AC_MSG_NOTICE([Missing required packages: $gal_missing_required])
  AC_MSG_ERROR([Aborting configuration])
fi
])


# _GAL_WITH_PACKAGE(name, help, path, file)
#   Wrapper for AC_ARG_WITH and AC_CACHE_CHECK
#   Provides a --with-name option with help.
#   If package location not given, try to find `file' in `path' and
#   keep the *last* match
#
AC_DEFUN([_GAL_WITH_PACKAGE], 
[AC_ARG_WITH([$1], [$2],
             [_gal_with_$1=$withval])
AC_CACHE_CHECK([for $1],
               [gal_cv_root_$1],
               [if test x$_gal_with_$1 = xno
                then
                  gal_cv_root_$1=no
                fi
                if test x$_gal_with_$1 = x || test x$_gal_with_$1 = xyes
                then
                  for p in $3
                  do
                    test -f $p/$4 && gal_cv_root_$1=$p
                  done
                  test x$gal_cv_root_$1 = x && gal_cv_root_$1=no
                else
                  gal_cv_root_$1=$_gal_with_$1
                fi
               ])
])


#
# _GAL_CHECK_LIB(libname, libpath, action_if_found, action_if_not_found)
#
# just look for the existence of the file "lib$1.$gal_dylib_extension" or "lib$1.a"
# 
# THIS SHOULD MAYBE MOVE TO REAL LINK CHECK, INSTEAD OF A SIMPLE 
# FILE EXISTENCE CHECK !!!
# 
AC_DEFUN([_GAL_CHECK_LIB], 
[AC_CHECK_FILE([$2/lib$1.$gal_dylib_extension],[$3],[AC_CHECK_FILE([$2/lib$1.a],[$3],[$4])])])
dnl AC_LANG_PUSH([C++])
dnl   _GAL_SAVE_COMPILE_FLAGS
dnl  CPPFLAGS="$BOOST_CPPFLAGS"
dnl  LDFLAGS="-L$BOOSTROOT/$2 -l$1"
dnl  AC_LINK_IFELSE(
dnl    [#include <boost/filesystem/operations.hpp>
dnl     #include <boost/filesystem/path.hpp>
dnl    int main (int argc, char **argv) {
dnl       namespace fs = boost::filesystem;
dnl       fs::path testPath ("nix", fs::native);
dnl       return 0;
dnl      }],[$3],[$4])])



# _GAL_BASENAME
#  M4 analogue to the basename shell utility
#
m4_define([_GAL_BASENAME], [m4_bregexp($1, [\(.*/\|\)\(.+\)], [\2])])

# _GAL_NORMALISE_VERSION_3
#   Convert dot-separated version number into a 
#   comma-separated list for individual processing
#   Up to 3 components accepted, missing componented
#   replaced by 0.
#
m4_define([_GAL_NORMALISE_VERSION_3], 
          [_GAL_NORMALISE_VERSION_AUX_3(m4_bpatsubst($1, [\.], [,]))])

m4_define([_GAL_NORMALISE_VERSION_AUX_3], 
  [ifelse($#, 3, [$1, $2, $3], $#, 2, [$1, $2, 0], $#, 1, [$1, 0, 0], [AC_FATAL([Maximal 3 components in version expected - got $*])])])

# _GAL_SAVE_COMPILE_FLAGS
#   Save flags for compilation so we can modify them for test compilation
#   NB: This macro cannot be nested
#
m4_define([_GAL_SAVE_COMPILE_FLAGS],
[_gal_save_CXXFLAGS="$CXXFLAGS"
_gal_save_CPPFLAGS="$CPPFLAGS"
_gal_save_LDFLAGS="$LDFLAGS"
_gal_save_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
])

# _GAL_SAVE_RESTORE_FLAGS
#   Restore flags after compilation. so we can modify them for test compilation
#
m4_define([_GAL_RESTORE_COMPILE_FLAGS],
[CXXFLAGS="$_gal_save_CXXFLAGS"
CPPFLAGS="$_gal_save_CPPFLAGS"
LDFLAGS="$_gal_save_LDFLAGS"
LD_LIBRARY_PATH="$_gal_save_LD_LIBRARY_PATH"
])

# GAL_WRITE_GLOBAL_REVISION
#  Since SVN does not provide a keyword for 
#  global revision number replacement, we 
#  compute it here using the svnversion command
#  and store the result in a singleton class 
#  (whose path and name is passed as argument).
#  This class can then be used by the framework to 
#  log or print out the global SVN revision number.  
#  This is meant particularly to assist people who
#  run directly from the SVN repository to specify what
#  exactly what they used during a run (eg. in case of bug reports)
#  In case of a tarball release, this revision number 
#  will be set to the global revision at the time the
#  release manager did the make dist.
#
AC_DEFUN([GAL_WRITE_GLOBAL_REVISION],
 [AC_CHECK_FILE( [.svn],
   SVN_REVISION=`svnversion .`
   [AC_MSG_NOTICE([Global SVN revision : $SVN_REVISION])]
   [sed 's+fGlobalId("[[^"]]*+fGlobalId("'"`svnversion`"'+' $1.in > $1],
   [AC_MSG_NOTICE([You are not working from SVN, so I will not update global revision number.])])]
)
