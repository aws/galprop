AC_PREREQ(2.59)
AC_INIT([GalProp], [54.1.2423], [])
AM_INIT_AUTOMAKE([-Wall -Werror foreign])

#AC_CONFIG_MACRO_DIR([m4])

# who am i
AC_CANONICAL_HOST

# Checks for programs.
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AM_PROG_AS
m4_ifdef([AM_PROG_AR], [AM_PROG_AR])

#Store CXXFLAGS so they can be reset later for overriding the defaults
save_cxxflags="${CXXFLAGS}"
save_fflags="${FFLAGS}"
AC_PROG_CXX
AC_PROG_F77
AC_PROG_FC

AC_PROG_LIBTOOL
AC_PROG_INSTALL

# Checks for header files.
AC_HEADER_STDC
AC_HEADER_DIRENT
AC_CHECK_HEADERS([signal.h errno.h])
AC_FUNC_ALLOCA

# Checks for typedefs, structures, and compiler characteristics.
AC_HEADER_STDBOOL
AC_C_CONST
AC_C_INLINE
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_HEADER_TIME
AC_CHECK_SIZEOF(void*)
AC_CHECK_TYPES([uint32_t, uint64_t])
AC_CHECK_SIZEOF(unsigned int)
AC_CHECK_SIZEOF(unsigned long)
AC_CHECK_SIZEOF(unsigned long long)

# Checks for library functions.
AC_PROG_GCC_TRADITIONAL
AC_FUNC_STAT
AC_CHECK_FUNCS([log exp pow sqrt])

# Initialise
GAL_INIT

# Find architecture
GAL_ARCH

# Enable/disable
AC_ARG_ENABLE(debug, AS_HELP_STRING([--enable-debug], [debug mode]))
#, [DEBUG=${enableval}])
AC_ARG_ENABLE(openmp, AS_HELP_STRING([--enable-openmp], [Multi-threaded parallel version using OpenMP]))

if test -n "${enable_openmp}" -a x"${enable_openmp}" != x"no"; then
  USE_OMP=1
fi
 
# Check for Darwin
GAL_CHECK_DARWIN

# Compiler flags
AC_LANG_PUSH(C++)
CXXFLAGS="${save_cxxflags}"
if test -n "${enable_debug}" -a x"${enable_debug}" != x"no"; then
   CXXFLAGS="-g"
else
   GAL_CXXFLAGS
fi
if ! echo $CXXFLAGS | grep -q -- -Wall ; then
  CXXFLAGS="$CXXFLAGS -Wall"
fi
AC_MSG_NOTICE([Using CXXFLAGS="$CXXFLAGS"])
AC_LANG_POP(C++)

AC_LANG_PUSH(Fortran 77)
FFLAGS="${save_fflags}"
if test -n "${enable_debug}" -a x"${enable_debug}" != x"no"; then
   FFLAGS="-g"
else
   GAL_FFLAGS
fi
if ! echo $FFLAGS | grep -q -- -Wall ; then
  FFLAGS="$FFLAGS -Wall"
fi
AC_MSG_NOTICE([Using FFLAGS="$FFLAGS"])
AC_LANG_POP(Fortran 77)

AC_F77_WRAPPERS
AC_F77_LIBRARY_LDFLAGS
acx_save_libs="${LIBS}"
LIBS="${FEXTRALIBS} $FLIBS"

if test "${USE_OMP}"; then

  AC_LANG_PUSH(C++)
  AC_DEFINE(USE_OMP, 1, [enable OpenMP support])
  GAL_OPENMP(,AC_MSG_ERROR([Compiler does not support OpenMP]))
  CXXFLAGS="$CXXFLAGS $OPENMP_CXXFLAGS"
  AC_LANG_POP(C++)

  AC_LANG_PUSH(Fortran 77)
  AC_DEFINE(USE_OMP, 1, [enable OpenMP support])
  GAL_OPENMP_FORTRAN(,AC_MSG_ERROR([Compiler does not support OpenMP]))
  FFLAGS="$FFLAGS $OPENMP_FFLAGS"
  AC_LANG_POP(Fortran 77)

fi

GAL_REQUIRE_CFITSIO
GAL_REQUIRE_CCFITS
GAL_REQUIRE_CLHEP
GAL_REQUIRE_GSL
GAL_REQUIRE_HEALPIX
#Check for healpix headers
AC_LANG_PUSH(C++)
CPPFLAGS_OLD=$CPPFLAGS
CPPFLAGS=$HEALPIX_CPPFLAGS
AC_SUBST(CPPFLAGS)
AC_CHECK_HEADERS([alm_healpix_tools.h lsconstants.h])
CPPFLAGS=$CPPFLAGS_OLD
AC_SUBST(CPPFLAGS)
AC_LANG_POP(C++)


GAL_WITH_GALDEF
GAL_WITH_FITSDATA

GAL_CHECK_MISSING_REQUIRED_PACKAGES

AC_CONFIG_HEADERS([config.h])

AC_CONFIG_FILES([Makefile source/Makefile])

AC_CONFIG_FILES([galprop-config])

AC_OUTPUT
GAL_FINISH

